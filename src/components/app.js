import React, { Component } from 'react';
import DrumMachine from './drum-machine';

export default class App extends Component {
  render() {
    return (
      <div className='container'>
        <DrumMachine />
        <footer>
          Display Font courtesy of <a href="https://fontstruct.com/fontstructions/show/101159/spell_a">Lord Nightmare on fontstruct</a>
        </footer>
      </div>
    );
  }
}
