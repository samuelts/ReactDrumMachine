import React, { Component } from 'react';

class Record extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <div className='recorder'>
        <button className='btn' id='record-btn' onClick={this.props.toggleRecord}><i class="fas fa-microphone-alt"></i></button>
        <button className='btn' id='play-btn' onClick={this.props.prePlayRec}><i class="fas fa-play"></i></button>
        <button className='btn' id='erase-btn' onClick={this.props.clearRec}><i class="fas fa-eraser"></i></button>
      </div>
    );
  }
}

export default Record;
