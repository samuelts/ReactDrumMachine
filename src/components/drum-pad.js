import React, { Component } from 'react';

export default class DrumPad extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.callPlay = this.callPlay.bind(this);
  }

  // Add event listeners on mount for keypress/release
  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress.bind(this));
    document.addEventListener('keyup', this.handleKeyUp.bind(this));
  }

  // Remove event listeners on unmount for keypress/release
  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress.bind(this));
    document.removeEventListener('keyup', this.handleKeyUp.bind(this));
  }

  // Set button active state on keypress
  handleKeyPress(event) {
    if (event.keyCode === this.props.keyCode) {
      document.getElementById(this.props.id).classList.add("btn-active");
      this.callPlay();
    }
  }

  // Remove button active state on key release
  handleKeyUp(event) {
    if (event.keyCode === this.props.keyCode) {
      document.getElementById(this.props.id).classList.toggle("btn-active");
    }
  }

  // Handle click function (separated from callPlay function for semantic reasons)
  handleClick() {
    this.callPlay();
  }

  // Call callback function to prep audio for play
  callPlay() {
    let audio = document.getElementById(this.props.keyTrigger);
    this.props.prepAudio(audio, this.props.id);
  }

  render() {
    return (
      <button
        className='drum-pad btn btn-secondary'
        onClick={this.handleClick.bind(this)}
        id={this.props.id}
      >
        {this.props.keyTrigger}
        <audio class='clip' id={this.props.keyTrigger} src={this.props.url}>{this.props.id}</audio>
      </button>
    );
  }
}