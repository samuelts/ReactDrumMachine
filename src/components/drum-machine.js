import React, { Component } from 'react';
import Display from './display';
import DrumPad from './drum-pad';
import Record from './record';

export default class DrumMachine extends Component {
  constructor(props) {
    super(props)

    this.state={
      clipName: 'DRUM MACHINE 9000',
      isPlaying: false,
      isRecording: false,
      recArr: [],
      i: 0
    }

    this.prepAudio = this.prepAudio.bind(this);
    this.playAudio = this.playAudio.bind(this);
    this.toggleRecord = this.toggleRecord.bind(this);
    this.prePlayRec = this.prePlayRec.bind(this);
    this.playRec = this.playRec.bind(this);
    this.clearRec = this.clearRec.bind(this);
  }

  prepAudio(audio, id) {
    // Remove event listener so it doesn't play any recorded audio on audio end
    audio.removeEventListener('ended', this.playRec);
    this.playAudio(audio, id);
  }

  playAudio(audio, id=null) {
    // Play audio clip
    audio.currentTime = 0;
    audio.play();

    // Update clipName
    if (id) {
      const clipName = id.split('-').join(' ');
      this.setState({
        clipName: clipName
      });
    }

    // If recording add audio to recording array
    if (this.state.isRecording) {
      this.setState({
        recArr: this.state.recArr.concat(audio)
      });
    }
  }

  toggleRecord() {
    //Toggles recording state and changes display
    if (this.state.isRecording) {
      this.setState({
        clipName: 'Stop Record',
        isRecording: false,
        i: 0
      });
      document.getElementById('record-btn').innerHTML = '<i class="fas fa-microphone-alt"></i>';
    } else {
      this.setState({
        clipName: 'Record',
        isRecording: true,
        i: 0
      });
      document.getElementById('record-btn').innerHTML = '<i class="fas fa-microphone-alt-slash"></i>';
    }
  }

  prePlayRec() {
    // If audio playing stop it
    // Otherwise continue

    if (this.state.isPlaying) {
      const audio = document.getElementsByClassName('clip');
      for (let i=0; i<audio.length; i++) {
        audio[i].pause();
        audio[i].currentTime = 0;
      }
      this.setState({
        clipName: 'Stop',
        isPlaying: false,
        i: 0
      });
      document.getElementById('play-btn').innerHTML= '<i class="fas fa-play"></i>';
      return;
    } else if (this.state.recArr.length !== 0) {
      this.setState({
        clipName: 'Play',
        isPlaying: true,
        i: 0
      });
      document.getElementById('play-btn').innerHTML= '<i class="fas fa-pause"></i>';
    } else {
      return;
    }
    this.playRec();

  }

  playRec() {
    let recArr = this.state.recArr;
    let i = this.state.i;

    // If at end of array, reset counter
    if (i+1 > recArr.length) {
      this.setState({
        clipName: 'Stop',
        isPlaying: false,
        i: 0
      });
      document.getElementById('play-btn').innerHTML= '<i class="fas fa-play"></i>';
      return;
    }

    // Step through recording array and get audio file
    this.setState({
      i: i+1
    });
    const audio = recArr[i];
    // Add event listener with recursive callback to ensure sequential playback
    audio.addEventListener('ended', this.playRec);

    // Play audio file
    this.playAudio(audio, audio.innerText);
  }

  clearRec() {
    // Clears recording array
    this.setState({
      clipName: 'Erased',
      recArr: []
    });
  }

  render() {
    const padRow1 = [
      {keyCode: 81,
        keyTrigger: 'Q',
        id: 'Heater-1',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3'},
      {keyCode: 87,
        keyTrigger: 'W',
        id: 'Heater-2',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-2.mp3'},
      {keyCode: 69,
        keyTrigger: 'E',
        id: 'Heater-3',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3'}];

    const padRow2 = [
      {keyCode: 65,
        keyTrigger: 'A',
        id: 'Heater-4',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3'},
      {keyCode: 83,
        keyTrigger: 'S',
        id: 'Clap',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3'},
      {keyCode: 68,
        keyTrigger: 'D',
        id: 'Open-HH',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3'}];

    const padRow3 = [
      {keyCode: 90,
        keyTrigger: 'Z',
        id: "Kick-n'-Hat",
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3'},
      {keyCode: 88,
        keyTrigger: 'X',
        id: 'Kick',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3'},
      {keyCode: 67,
        keyTrigger: 'C',
        id: 'Closed-HH',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Cev_H2.mp3'}];

    const drumpadRow1 = padRow1.map((pad) => {
      return (
        <DrumPad
          keyCode={pad.keyCode}
          keyTrigger={pad.keyTrigger}
          id={pad.id}
          url={pad.url}
          prepAudio={this.prepAudio}
        />
      );
    });

    const drumpadRow2 = padRow2.map((pad) => {
      return (
        <DrumPad
          keyCode={pad.keyCode}
          keyTrigger={pad.keyTrigger}
          id={pad.id}
          url={pad.url}
          prepAudio={this.prepAudio}
        />
      );
    });

    const drumpadRow3 = padRow3.map((pad) => {
      return (
        <DrumPad
          keyCode={pad.keyCode}
          keyTrigger={pad.keyTrigger}
          id={pad.id}
          url={pad.url}
          prepAudio={this.prepAudio}
        />
      );
    });

    return (
      <div className='wrapper' id='drum-machine'>

        <div className='display-wrapper'>
          <Display clipName={this.state.clipName} />
        </div>

        <div className='btn-wrapper'>
          <div id='row-1'>
            {drumpadRow1}
          </div>

          <div id='row-2'>
            {drumpadRow2}
          </div>

          <div id='row-3'>
            {drumpadRow3}
          </div>
        </div>

        <div className='record-wrapper'>
          <Record
            toggleRecord={this.toggleRecord}
            prePlayRec={this.prePlayRec}
            clearRec={this.clearRec}
          />
        </div>
      </div>
    );
  }
}